// Repositories contain the functionalities (CRUD)

package com.zuitt.wdc044.repositories;

import com.zuitt.wdc044.models.Post;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


// @Repository -- interface that has marked of @Repository contains methods for database manipulation
// from the third imported package
// by inheriting CrudRepository, PostRepository has inherited its predefined methods for creating, retrieving, updating, and even deleting records. We can actually perform CRUD operations
@Repository

public interface PostRepository extends CrudRepository<Post, Object>{

}
