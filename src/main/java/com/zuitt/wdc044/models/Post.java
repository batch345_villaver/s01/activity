package com.zuitt.wdc044.models;

import javax.persistence.*;

// this @Entity annotation is from the javax.persistence package
// mark this Java object as a representation of a database table via @Entity annotation
@Entity

// @Table designates the name of the tables
// in plural form
@Table(name = "posts")
public class Post {
    // @Id indicates that this property represents the primary key
    @Id

    // the values for this property will be auto-incremented
    @GeneratedValue

    // 1. Properties
    // this is the primary key, and will be auto-incremented
    private Long id;

    // adding columns
    // class properties that represent table columns in a relational database, and is annotated by Column
    @Column

    // adding the Title column
    private String title;

    // adding another column
    @Column
    private String content;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }


    // 2. Constructors
    // empty
    public Post(){};

    // parameterized
    public Post(String title, String content){
        this.title = title;
        this.content = content;
    }

    public Post (String title, String content, User user) {
        this.title = title;
        this.content = content;
        this.user = user;
    }


    // 3. Getters and Setters
    // no need to make getters and setters for the Id
    public String getTitle(){
        return this.title;
    }

    public String getContent(){
        return this.content;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public void setContent(String content){
        this.content = content;
    }
}
