package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;
import org.springframework.http.ResponseEntity;

public interface PostService {
    void createPost(String stringToken, Post post);

    // get all the posts
    Iterable<Post> getPosts();

    // updating a post
    ResponseEntity<?> updatePost(Long id, String stringToken, Post post);

    // deleting a post
    ResponseEntity<?> deletePost(Long id, String stringToken);

    Iterable<Post> getMyPosts(String stringToken);
}
