package com.zuitt.wdc044.services;

// We need to have access directly to the users table through the User model
import com.zuitt.wdc044.models.User;

// We need to have access to the UserRepository because we will be needing the methods inside the said repository
import com.zuitt.wdc044.repositories.UserRepository;

// user @Autowired annotation to use the UserRepository
import org.springframework.beans.factory.annotation.Autowired;

// This object contains the userdetails
import org.springframework.security.core.userdetails.UserDetails;

// Interface
import org.springframework.security.core.userdetails.UserDetailsService;

// the @Component annotation declares that our class is a component class
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;


@Component
public class JwtUserDetailsService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    @Override
    // Will return an Object data type that contains the UserDetails
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);

        // if user does not exist (existential crisis?)
        if (user == null) {
            throw new UsernameNotFoundException("User not found with the username: " + username);
        }

        // if user merely exists, and does not live
        // getUsername() and getPassword() are used since both are in private
        // new ArrayList<>() is where the username and password are being contained
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), new ArrayList<>());
    }
}
