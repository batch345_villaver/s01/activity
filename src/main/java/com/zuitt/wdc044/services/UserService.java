package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.User;
import java.util.Optional;

public interface UserService {
    // we just created a default service
    // User indicates the data type
    // user -- argument. instantiated object
    void createUser(User user);

    // Optional -- if the username is found, it will return the whole record that matches the username, otherwise, it will return null
    // We just created a default service wherein it will return the record that will match the username provided in the parameter, otherwise, Optional will return null
    Optional<User> findByUsername(String username);
}
